import React  ,{ Component}from  'react';
import SampleCard from './SampleCard';
import Listbutton from './Listbutton';
import InputField from './InputField';
//import Cardimages from './Cardimages';

class ParentComponent extends Component {
  constructor(props){
    super(props);
    this.state={
        name :'',
        phone:'',
        email:''
    }
}
handle = (e) =>{
  this.setState({[e.target.name]:e.target.value})
  
  }
  hello = (e)=>{
    e.preventDefault();
    alert( this.state.name);
  
  }
  
  secondf = (e) =>{
    e.preventDefault();
    console.log(this.state.email);

  }
  
  thirdf = (e) =>{
    e.preventDefault();
    alert(this.state.phone);
  }

  
  render() {
    return(
        <>
    <SampleCard 
    
        cardColClass={"col-sm-3 mt-2"}
        cardBorder={"card border-default mx-1"}
        cardTitle={"First Card"}
        cardText={" Parentcomponentjs edit selection views ??? "}
    > 
    <InputField placeholder={"Enter Your Name"}
    type={"text"} onChange={this.handle}
    name={"name"}
    class={"form-control"} auto={"Off"}
    >
    </InputField>
    <Listbutton className={" btn btn-outline-primary me-1 mb-1 mt-1"} onClick={this.hello} name={"Submit"}></Listbutton>
    </SampleCard>

    <SampleCard 
        cardColClass={"col-sm-3 mt-2"}
        cardBorder={"card border-default mx-1"}
        cardTitle={"Second Card"}
        cardText={" Parentcomponentjs edit selection views. "}
    > 
    <InputField placeholder={"Enter Your Name"}
    type={"text"} onChange={this.handle}
    name={"email"}
    class={"form-control"} auto={"Off"}
    >
    </InputField>
    <Listbutton className={" btn btn-outline-success me-1 mb-1 mt-1"} onClick={this.secondf} name={"Login"}></Listbutton>
    </SampleCard>
    
    <SampleCard 
        cardColClass={"col-sm-3 mt-2"}
        cardBorder={"card border-default mx-1"}
        cardTitle={"Third Card"}
        cardText={" Parentcomponentjs edit selection views. "}
    > 
    <InputField placeholder={"Enter Your Name"}
    type={"text"} onChange={this.handle}
    name={"phone"}
    class={"form-control"} auto={"Off"}
    >
    </InputField>
    <Listbutton className={" btn btn-outline-danger me-1 mb-1 mt-1"} onClick={this.thirdf} name={"Register"}></Listbutton>
    </SampleCard>
    
    
     </>
      
    )
  }
}
export default ParentComponent;
