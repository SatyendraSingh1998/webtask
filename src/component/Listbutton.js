import React from 'react'

const Listbutton = (props) => {
   
  return (
    <>
   
   <button className={props.className} onClick={props.onClick}> {props.name} </button> 
  </>
  )
}

export default Listbutton
