import React from 'react';
//import ParentComponent from './component/ParentComponent';
import { Route ,Switch } from 'react-router-dom';
import './assets/css/style.css';
//import Listview from './yestaerdaytask/Listview';
import Showhere from './yestaerdaytask/Showhere';
import Login from './yestaerdaytask/Login';
import Forgatepass from './yestaerdaytask/Forgatepass';
import Dasboard from './yestaerdaytask/Dasboard';
import Listview from './yestaerdaytask/Listview';

function App() {
  return (
    <>
    {/* <Listview /> */}
   {/* <ParentComponent /> */}
    <Switch>
      <Route path="/" exact component={Showhere} />
      <Route exact path="/Listview"><Listview /></Route>
      <Route exact path="/Login"><Login /></Route>
      <Route exact path="/Dasboard"><Dasboard /></Route>
      <Route exact path="/Forgatepass"><Forgatepass /></Route>
    </Switch>
    
   </>
     );
}

export default App;
