import React from 'react'

const Listview = () => {
  return (
    <>
      <div className="appHeader bg-primary text-light">
        <div className="left">
            <a href="Task" className="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div className="pageTitle"> Multi Listview</div>
        <div className="right">
        </div>
    </div>
    <div id="appCapsule">
    <div className="listview-title mt-2">Simple Multi Listview</div>
        <ul className="listview link-listview">
            <li>
                <a href="task">Link List</a>
                </li>
            <li>
                <a href="task">
                    Simple List
                </a>
            </li>
            <li>
                <a href="task">
                    Imaged List
                   
                </a>
            </li>
            <li>
                <a href="task">
                    Iconed List
                    
                </a>
            </li>
            <li>
                <a href="task">
                   Media List
                   
                </a>
            </li>
        </ul>
        <div className="listview-title mt-2">Imaged multi Listview</div>
        <ul className="listview image-listview">
            <li>
                <a href="task" className="item">
                    <img src="https://media.wired.com/photos/5ed59fb9609ca7f4d498984f/4:3/w_2131,h_1598,c_limit/business_facebook_1178141613.jpg" alt="imag" className="image"/>
                    <div className="in">
                        <div>Simple List</div>
                       
                    </div>
                </a>
            </li>
            <li>
                <a href="task" className="item">
                    <img src="https://media.wired.com/photos/5e31ffb79cc92c00080b91ae/master/w_1600%2Cc_limit/WI030120_FF_Zuck_03.jpg" alt="imag" className="image"/>
                    <div className="in">
                        <div>Link List</div>
                       
                    </div>
                </a>
            </li>
            <li>
                <a href="task" className="item">
                    <img src="https://techstory.in/wp-content/uploads/2018/10/1_QeOrf-n5NAdxp0AxHIGlsw.jpeg" alt="imag" className="image"/>
                    <div className="in">
                        <div>imaged Link</div>
                    </div>
                </a>
            </li>
        </ul>
        <div className="appBottomMenu">
        <a href="index.html" className="item">
            <div className="col">
                <ion-icon name="home-outline"></ion-icon>
            </div>
        </a>
        <a href="app-components.html" className="item">
            <div className="col">
                <ion-icon name="cube-outline"></ion-icon>
            </div>
        </a>
        <a href="page-chat.html" className="item">
            <div className="col">
                <ion-icon name="chatbubble-ellipses-outline"></ion-icon>
                <span className="badge badge-danger">5</span>
            </div>
        </a>
        <a href="app-pages.html" className="item">
            <div className="col">
                <ion-icon name="layers-outline"></ion-icon>
            </div>
        </a>
        <a href="k" className="item" data-bs-toggle="modal" data-bs-target="#sidebarPanel">
            <div className="col">
                <ion-icon name="menu-outline"></ion-icon>
            </div>
        </a>
    </div>
        </div>
        
    </>
  )
}

export default Listview
